<?php

/**
 * @file
 * Batch processing functions for user_history records.
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\user_history\Entity\UserHistory;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Add fields to user_history records.
 *
 * Function to add one or more fields to the user_history entity to allow
 * tracking of changes to the same field on the user entity.  This function
 * will be executed in a batch context.
 *
 * @param array $tracked_fields
 *   The list of fields to be tracked.
 * @param array $context
 *   The batch processing context.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_history_add_tracked_fields(array $tracked_fields, array &$context) {

  // Get the field definitions for fields attached to user entity.
  $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');

  // Get the field config from user fields for each field to be added,
  // and add fields to user_history entity.
  foreach ($tracked_fields as $field_name) {

    /** @var \Drupal\field\Entity\FieldConfig $user_field */
    $user_field = $field_definitions[$field_name];

    $field_storage = FieldStorageConfig::loadByName('user_history', $field_name);
    if (empty($field_storage)) {
      // Get field storage values from user field and adjust for user_history.
      $field_storage_values = $user_field->getFieldStorageDefinition()->toArray();
      unset($field_storage_values['uuid']);
      unset($field_storage_values['id']);
      $field_storage_values['entity_type'] = 'user_history';
      $field_storage_values['module'] = 'user_history';
      $field_storage_values['dependencies'] = [
        'module' => ['user_history'],
      ];
      // Create the field storage config entity.
      $field_storage = FieldStorageConfig::create($field_storage_values);
      $field_storage->save();
    }

    $field_config = FieldConfig::loadByName('user_history', 'user_history', $field_name);
    if (empty($field_config)) {
      // Get field instance values from user field and adjust for user_history.
      $field_config_values = $user_field->toArray();
      unset($field_config_values['uuid']);
      unset($field_config_values['id']);

      $field_config_values['field_storage'] = $field_storage;
      $field_config_values['entity_type'] = 'user_history';
      $field_config_values['bundle'] = 'user_history';
      $field_config_values['dependencies'] = [
        'config' => ['field.storage.' . $field_storage->id()],
        'module' => ['user_history'],
      ];
      // Create the field instance config entity.
      $field_config = FieldConfig::create($field_config_values);
      $field_config->save();
    }

    // Log message about new field added.
    \Drupal::logger('user_history')->info('Field %label (%name) added to user_history entity.',
      ['%label' => $field_config->getLabel(), '%name' => $field_name]);

  }

  // Update the details of the running config.
  $config = \Drupal::config('user_history.settings');
  \Drupal::state()->set('user_history.base_fields', $config->get('base_fields'));
  \Drupal::state()->set('user_history.attached_fields', $config->get('attached_fields'));

}

/**
 * Remove field from user_history record.
 *
 * Function to remove a field from the user_history entity which is no longer
 * required to be tracked.  This function will be executed in a batch context.
 *
 * @param array $remove_fields
 *   List of fields to remove.
 * @param array $context
 *   Batch processing context.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_history_remove_tracked_fields(array $remove_fields, array &$context) {
  \Drupal::messenger()->addMessage(t('Remove fields from user_history: %fields', ['%fields' => implode(', ', $remove_fields)]));

  // Delete the field instance config and field storage config for each field
  // to be removed.
  foreach ($remove_fields as $field_name) {

    $field_config = FieldConfig::loadByName('user_history', 'user_history', $field_name);
    if (!empty($field_config)) {
      $field_config->delete();
    }

    $field_storage = FieldStorageConfig::loadByName('user_history', $field_name);
    if (!empty($field_storage)) {
      $field_storage->delete();
    }

    // Log message about existing field removed.
    \Drupal::logger('user_history')->info('Field %label (%name) removed from user_history entity.',
      ['%label' => $field_config->getLabel(), '%name' => $field_name]);

  }

  // Update the details of the running config.
  $config = \Drupal::config('user_history.settings');
  \Drupal::state()->set('user_history.base_fields', $config->get('base_fields'));
  \Drupal::state()->set('user_history.attached_fields', $config->get('attached_fields'));

}

/**
 * Create initial user_history records.
 *
 * Function to create initial user_history records for all users currently in
 * the database.  This function will be executed in a batch context.
 *
 * @param int $batch_size
 *   The number of user accounts to process in each batch run.
 * @param array $context
 *   The batch processing context.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_history_create_initial_history(int $batch_size, array &$context) {

  $entity_type = 'user_history';
  $values = [];
  $current_user = \Drupal::currentUser();

  $config = \Drupal::config('user_history.settings');
  // Get a list of fields that should be tracked by the user_history entity.
  $attached_fields = $config->get('attached_fields');
  $tracked_fields = [];
  foreach ($attached_fields as $field_name => $tracked) {
    if ($tracked) {
      $tracked_fields[] = $field_name;
    }
  }

  // Initialise the sandbox values on the first call.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = count(\Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('uid', 0, '>')
      ->execute());
    $context['sandbox']['last'] = 0;
    $context['sandbox']['count'] = 0;
    // Get current time for entity labels.
    $now = new DrupalDateTime('now', date_default_timezone_get());
    $context['sandbox']['date'] = $now->format('Ymd-Hi');
    // Set the install date.
    \Drupal::state()->set('user_history.install_date', $now->format('Y-m-d H:i'));
  }

  // Retrieve the next set of user accounts.
  $uids = \Drupal::entityQuery('user')
    ->accessCheck(FALSE)
    ->condition('uid', $context['sandbox']['last'], '>')
    ->sort('uid')
    ->range(0, $batch_size)
    ->execute();

  $accounts = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple($uids);

  /** @var \Drupal\user\Entity\User $account */
  foreach ($accounts as $account) {
    // Ignore the anonymous account.
    if (!$account->isAnonymous()) {

      $user_history = new UserHistory($values, $entity_type);

      // Set the label for this user_history entity.
      $user_history->setLabel($account->id() . ': Install - ' . $context['sandbox']['date']);

      // Set the modified time to the last changed time of the account.
      $user_history->setCreatedTime($account->getChangedTime() ?? 0);
      // Set the modified_by uid value to current user.
      $user_history->setModifiedByUid($current_user->id());
      // Record that this record was generated during module install.
      $user_history->setAction('Install');
      // Record that this user entity exists in the database.
      $user_history->setUserDeleted(FALSE);

      $user_history->setUserId($account->id() ?? 0);
      $user_history->setUsername($account->getAccountname() ?? '');
      $user_history->setUserPass($account->getPassword() ?? '');
      $user_history->setUserMail($account->getEmail() ?? '');
      $user_history->setUserTimezone($account->getTimeZone() ?? '');
      $user_history->setUserStatus($account->isActive() ?? FALSE);
      $user_history->setUserRoles(implode('; ', $account->getRoles(TRUE) ?? []));
      $user_history->setUserCreated($account->getCreatedTime() ?? 0);
      $user_history->setUserChanged($account->getChangedTime() ?? 0);
      $user_history->setUserAccess($account->getLastAccessedTime() ?? 0);
      $user_history->setUserLogin($account->getLastLoginTime() ?? 0);
      $user_history->setUserInit($account->getInitialEmail() ?? '');
      $user_history->setUserLangcode($account->language()->getId() ?? '');
      $user_history->setUserPreferredLangcode($account->getPreferredLangcode() ?? '');
      $user_history->setUserPreferredAdminLangcode($account->getPreferredAdminLangcode() ?? '');

      // Get a list of fields attached to the user_history entity.
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user_history', 'user_history');
      // Add values of tracked fields attached to the user entity.
      foreach ($tracked_fields as $field_name) {

        /** @var \Drupal\field\Entity\FieldConfig $field_definition */
        $field_definition = $field_definitions[$field_name];
        // $field_type is not currently used.
        // Present for development/debug purposes.
        /** @noinspection PhpUnusedLocalVariableInspection */
        $field_type = $field_definition->getType();

        /** @var \Drupal\Core\Field\FieldItemList $field_item_list */
        $field_item_list = $account->get($field_name);

        $history_values = user_history_get_tracked_field_value($field_item_list);
        $user_history->set($field_name, $history_values);
      }
      // Mark this user_history as a new entity.
      $user_history->enforceIsNew(TRUE);

      $user_history->save();

    }

    $context['sandbox']['last'] = $account->id();
    $context['sandbox']['count']++;
  }

  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user accounts.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;
    $context['message'] = t('Finished processing %total user accounts.', [
      '%total' => $context['sandbox']['total'],
    ]);
    $context['results']['total'] = $context['sandbox']['total'];
  }

}

/**
 * Batch processing finish callback.
 *
 * Completion function for batch operation to initialise the user_history
 * records.
 *
 * @param bool $success
 *   The result of the batch operation.
 * @param array $results
 *   The list of processed user ids.
 * @param array $operations
 *   The list of batch operations performed.
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 *   The next page to display to the user.
 */
function user_history_finished_initial_history(bool $success, array $results, array $operations) {

  $messenger = \Drupal::messenger();

  if ($success) {
    // Flag the initialisation as complete.
    \Drupal::state()->set('user_history.initialise_required', FALSE);
    // Display a completion message to the user.
    \Drupal::messenger()->addMessage(t('Added %total new user_history records', ['%total' => $results['total']]));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $messenger
      ->addMessage(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
  }

  // Redirect the user to the system status report.
  return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());

}

/**
 * Update user_history tracked fields.
 *
 * Function to update user_history records when a tracked field is added or
 * removed from the history processing.  This function is executed in a batch
 * context.
 *
 * @param int $batch_size
 *   The number of user accounts to process in each batch run.
 * @param array $add_fields
 *   The list of fields to add to tracking.
 * @param array $remove_fields
 *   The list of fields to remove from tracking.
 * @param array $context
 *   The batch processing context.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_history_update_tracked_history(int $batch_size, array $add_fields, array $remove_fields, array &$context) {

  $entity_type = 'user_history';
  $values = [];
  $current_user = \Drupal::currentUser();

  $config = \Drupal::config('user_history.settings');
  // Get a list of fields that should be tracked by the user_history entity.
  $attached_fields = $config->get('attached_fields');
  $tracked_fields = [];
  foreach ($attached_fields as $field_name => $tracked) {
    if ($tracked) {
      $tracked_fields[] = $field_name;
    }
  }

  // Initialise the sandbox values on the first call.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = \Drupal::entityQuery('user')->accessCheck(FALSE)->condition('uid', 0, '>')->count()->execute();
    $context['sandbox']['last'] = 0;
    $context['sandbox']['count'] = 0;
    $context['sandbox']['updated'] = 0;
    // Get current time for entity labels.
    $now = new DrupalDateTime('now', date_default_timezone_get());
    $context['sandbox']['date'] = $now->format('Ymd-Hi');
  }

  // Retrieve the next set of user accounts.
  $uids = \Drupal::entityQuery('user')
    ->accessCheck(FALSE)
    ->condition('uid', $context['sandbox']['last'], '>')
    ->sort('uid')
    ->range(0, $batch_size)
    ->execute();

  $accounts = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple($uids);

  /** @var \Drupal\user\Entity\User $account */
  foreach ($accounts as $account) {
    // Ignore the anonymous account.
    if (!$account->isAnonymous()) {

      // Set flag to initially suppress writing a user_history record to
      // database.
      $user_history_update = FALSE;

      // Create a new user_history entity.
      $user_history = new UserHistory($values, $entity_type);

      // Empty arrays for list of new fields added or old fields removed from
      // the user_history record for this account.
      $new_fields = [];
      $old_fields = [];

      // Get a list of fields attached to the user_history entity.
      $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('user_history', 'user_history');
      // Process each of these defined fields.
      /** @var \Drupal\field\Entity\FieldConfig $field_definition */
      foreach ($field_definitions as $field_name => $field_definition) {
        // Ignore base field definitions and process only attached fields.
        if ($field_definition instanceof FieldConfig) {
          /** @var \Drupal\Core\Field\FieldItemList $field_item_list */
          $field_item_list = $account->get($field_name);

          // Record the value (if any) of the attached field in the
          // user_history record?
          if (!empty($field_item_list)) {
            // $field_type is not currently used.
            // Present for development/debug purposes.
            /** @noinspection PhpUnusedLocalVariableInspection */
            $field_type = $field_definition->getType();

            // Check if account has a value for the field being removed from
            // tracking.
            if (in_array($field_name, $remove_fields) && $field_item_list->count() > 0) {
              // Yes, so set a flag to save a user_history record and record
              // this as a removed field.
              $user_history_update = TRUE;
              $old_fields[] = $field_definition->getLabel();
            }
            elseif (in_array($field_name, $add_fields) && $field_item_list->count() > 0) {
              // Account has a value for a field which is being added to
              // tracking,
              // so extract the values and add to a user_history record.
              $history_values = user_history_get_tracked_field_value($field_item_list);
              if (!empty($history_values)) {
                // Set a flag to create a new user_history record this as a
                // new attached field.
                $user_history_update = TRUE;
                $user_history->set($field_name, $history_values);
                $new_fields[] = $field_definition->getLabel();
              }
            }
            elseif (in_array($field_name, $tracked_fields) && $field_item_list->count() > 0) {
              // Account has a value for field which was already being tracked,
              // so extract the values and add to a user_history record.
              $history_values = user_history_get_tracked_field_value($field_item_list);
              if (!empty($history_values)) {
                // Do not set a flag to create a new user_history record,
                // or record the field name as being added or removed.
                $user_history->set($field_name, $history_values);
              }
            }
          }
        }
      }

      $difference = [];
      if (!empty($new_fields)) {
        $difference[] = 'Add field values: ' . implode(', ', $new_fields);
      }
      if (!empty($old_fields)) {
        $difference[] = 'Remove field values: ' . implode(', ', $old_fields);
      }

      // If the user_history entry contains values for any attached fields,
      // then populate the base fields and save.
      if ($user_history_update) {
        // Set the label for this user_history entity.
        $user_history->setLabel($account->id() . ': Modify - ' . $context['sandbox']['date']);

        // Set the modified time to the last changed time of the account.
        $user_history->setCreatedTime($account->getChangedTime() ?? 0);
        // Set the modified_by uid value to current user.
        $user_history->setModifiedByUid($current_user->id());
        // Record that this record was generated during tracked field update.
        $user_history->setAction('Modify tracked fields');
        // Record that this user entity exists in the database.
        $user_history->setUserDeleted(FALSE);

        $user_history->setUserId($account->id() ?? 0);
        $user_history->setUsername($account->getAccountname() ?? '');
        $user_history->setUserPass($account->getPassword() ?? '');
        $user_history->setUserMail($account->getEmail() ?? '');
        $user_history->setUserTimezone($account->getTimeZone() ?? '');
        $user_history->setUserStatus($account->isActive() ?? FALSE);
        $user_history->setUserRoles(implode('; ', $account->getRoles(TRUE) ?? []));
        $user_history->setUserCreated($account->getCreatedTime() ?? 0);
        $user_history->setUserChanged($account->getChangedTime() ?? 0);
        $user_history->setUserAccess($account->getLastAccessedTime() ?? 0);
        $user_history->setUserLogin($account->getLastLoginTime() ?? 0);
        $user_history->setUserInit($account->getInitialEmail() ?? '');
        $user_history->setUserLangcode($account->language()->getId() ?? '');
        $user_history->setUserPreferredLangcode($account->getPreferredLangcode() ?? '');
        $user_history->setUserPreferredAdminLangcode($account->getPreferredAdminLangcode() ?? '');

        // Record the reason for adding this user_history record.
        $user_history->setDifference(implode('. ', $difference));

        // Mark this user_history as a new entity.
        $user_history->enforceIsNew(TRUE);
        $user_history->save();
        $context['sandbox']['updated']++;
      }

    }

    $context['sandbox']['last'] = $account->id();
    $context['sandbox']['count']++;
  }

  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user accounts.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;
    $context['message'] = t('Finished processing %total user accounts. Added %num new user history records', [
      '%total' => $context['sandbox']['total'],
      '%num' => $context['sandbox']['updated'],
    ]);
    $context['results']['total'] = $context['sandbox']['total'];
    $context['results']['updated'] = $context['sandbox']['updated'];
  }

}

/**
 * Batch update processing finished callback.
 *
 * @param bool $success
 *   The result of the batch operation.
 * @param array $results
 *   The list of processed user ids.
 * @param array $operations
 *   The list of batch operations performed.
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 *   The next page to display to the user.
 */
function user_history_finished_update_attached_fields(bool $success, array $results, array $operations) {

  $messenger = \Drupal::messenger();

  if ($success) {
    // Flag the update as complete.
    \Drupal::state()->set('user_history.base_fields_update_required', FALSE);
    \Drupal::state()->set('user_history.attached_fields_update_required', FALSE);
    // Display a completion message to the user.
    \Drupal::messenger()->addMessage(t('Added %num new user_history records', ['%num' => $results['updated']]));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $messenger
      ->addMessage(t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
  }

  // Redirect to the system status report page.
  return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());

}
