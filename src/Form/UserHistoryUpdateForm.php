<?php

namespace Drupal\user_history\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the Batch Install Form.
 */
class UserHistoryUpdateForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Constructs a new UserHistorySettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state api service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, StateInterface $state) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_history_batch_update';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // If the user history records have not yet been initialised, then re-direct
    // the user to the initialise form instead.
    if ($this->state->get('user_history.initialise_required')) {
      return new RedirectResponse(Url::fromRoute('user_history.batch_install_form')->setAbsolute()->toString());
    }

    $config = $this->configFactory->get('user_history.settings');

    $entity_query = $this->entityTypeManager->getStorage('user')->getQuery()->accessCheck(FALSE);
    $message_args = [
      '%count' => $entity_query->condition('uid', 0, '>')->count()->execute(),
      ':settings' => Url::fromRoute('user_history.settings')->toString(),
    ];

    $form['notice'] = [
      '#markup' => '<div>' . $this->t('This process will update the user history records with values from fields attached to the user entities in the database.<br/>To change the fields to be added or removed, CANCEL this update and go to the <a href=":settings">User history settings</a> form.<br/>There are currently %count user records to be processed.', $message_args) . '</div>',
    ];

    // Get a list of fields attached to the user entity.
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $field_options = [];

    // Get a list of fields that should be tracked by the user_history entity.
    $tracked_fields = [];
    foreach ($config->get('attached_fields') as $field_name => $tracked) {
      if ($tracked) {
        $tracked_fields[] = $field_name;
      }
      /** @var \Drupal\field\Entity\FieldConfig $field_definition */
      $field_definition = $field_definitions[$field_name];
      $field_options[$field_name] = $field_definition->getLabel();
    }

    // Get a list of fields currently attached to the user_history entity.
    $current_fields = [];
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('user_history', 'user_history');
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($field_definition instanceof FieldConfig) {
        // Field added through UI or programmatically.
        $current_fields[] = $field_name;
        $field_options[$field_name] .= ' (currently tracked)';
      }
    }

    $add_fields = array_diff($tracked_fields, $current_fields);
    $remove_fields = array_diff($current_fields, $tracked_fields);

    if (!empty($add_fields)) {
      $form['add_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Add fields'),
        '#description' => $this->t('The above fields will be added to user history tracking.'),
        '#options' => $field_options,
        '#default_value' => $add_fields,
        '#disabled' => TRUE,
      ];
    }

    if (!empty($remove_fields)) {
      $form['remove_fields'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Remove fields'),
        '#description' => $this->t('The above fields will be removed from user history tracking.'),
        '#options' => $field_options,
        '#default_value' => $remove_fields,
        '#disabled' => TRUE,
      ];
    }

    $form['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('Enter the number of user accounts to process in each batch'),
      '#default_value' => 100,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Batch'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $batch_size = (int) $form_state->getValue('batch_size');
    if ($batch_size < 1 || $batch_size > 1000) {
      $form_state->setErrorByName('batch_size', $this->t('Invalid batch size. Specify an integer between 1 and 1000.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $batch_size = $form_state->getValue('batch_size');
    $add_fields = $form_state->getValue('add_fields');
    $add_fields = is_array($add_fields) ? array_filter($add_fields) : [];
    $remove_fields = $form_state->getValue('remove_fields');
    $remove_fields = is_array($remove_fields) ? array_filter($remove_fields) : [];

    $module_path = $this->moduleHandler->getModule('user_history')->getPath();

    $batch = [
      'title' => $this->t('Updating attached fields for user history records...'),
      'operations' => [],
      'init_message' => $this->t('Commencing'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing'),
      'finished' => 'user_history_finished_update_attached_fields',
      'file' => $module_path . '/user_history.batch.inc',
    ];

    if (!empty($add_fields)) {
      // Batch operation to define the new fields attached to the user_history
      // entity.
      $batch['operations'][] = [
        'user_history_add_tracked_fields', [
          $add_fields,
        ],
      ];
    }

    // Batch operation to update the user history records with changed field
    // data.
    $batch['operations'][] = [
      'user_history_update_tracked_history', [
        $batch_size,
        $add_fields,
        $remove_fields,
      ],
    ];

    if (!empty($remove_fields)) {
      // Batch operation to delete the old fields that were attached to
      // user_history entity.
      $batch['operations'][] = [
        'user_history_remove_tracked_fields', [
          $remove_fields,
        ],
      ];
    }

    batch_set($batch);

  }

}
