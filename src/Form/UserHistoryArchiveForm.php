<?php

namespace Drupal\user_history\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the Batch Install Form.
 */
class UserHistoryArchiveForm extends FormBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Constructs a new UserHistorySettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state api service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, StateInterface $state) {
    $this->entityFieldManager = $entity_field_manager;
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_history_batch_archive';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // If the user history records have not yet been initialised, then re-direct
    // the user to the initialise form instead.
    if ($this->state->get('user_history.initialise_required')) {
      return new RedirectResponse(Url::fromRoute('user_history.batch_install_form')->setAbsolute()->toString());
    }

    /** @noinspection PhpUnusedLocalVariableInspection */
    $config = $this->configFactory->get('user_history.settings');

    $message_args = [];

    $form['notice'] = [
      '#markup' => '<div>' . $this->t('This process will archive specified user history records by copying the details held in the database to an XML or CSV file<br/>and then deleting the records from the database.  Note that if a user account has only a single history record<br/>in the database, then it will not be archived or deleted.', $message_args) . '</div>',
    ];

    // Select a date for archiving records.
    $default_date = $form_state->getValue('archive_date');
    if (empty($default_date)) {
      $default_date = [
        'year' => date('Y') - 1,
        'month' => date('m'),
        'day' => date('d'),
      ];
    }
    $form['archive_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Archive date'),
      '#description' => $this->t('Archive all user history records older than or equal to this date.'),
      '#default_value' => $default_date,
    ];

    // Select a maximum number of history records to retain for each account.
    $form['min_records'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Minimum records'),
      '#description' => $this->t('Specify the minimum number of history records to be retained for each account'),
      '#default_value' => $form_state->getValue('min_records') ?? '1',
    ];

    // Select a maximum number of history records to retain for each account.
    $form['max_records'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum records'),
      '#description' => $this->t('Specify the maximum number of history records to be retained for each account'),
      '#default_value' => $form_state->getValue('max_records') ?? '10',
    ];

    // Define the formats able to be used for the archive file.
    $file_formats = [
      'txt' => 'TXT - serialized string',
      'csv' => 'CSV - comma separated values',
    ];
    if ($this->moduleHandler->moduleExists('serialization')) {
      $file_formats['xml'] = 'XML - structured data';
      $file_formats['json'] = 'JSON - structured data';
    }

    $form['file_format'] = [
      '#type' => 'radios',
      '#title' => $this->t('File format'),
      '#description' => $this->t('Select the format for the archive file'),
      '#options' => $file_formats,
      '#default_value' => isset($file_formats['xml']) ? 'xml' : 'csv',
      '#required' => TRUE,
    ];

    $form['delete_records'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete records'),
      '#description' => $this->t('Select this box to have the records deleted from the database after successful archive.'),
      '#default_value' => FALSE,
    ];

    $form['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('Enter the number of user accounts to process in each batch'),
      '#default_value' => 100,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Batch'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($min_records = $form_state->getValue('min_records'))) {
      $min_records = (int) $min_records;
      if ($min_records < 1 || $min_records > 999) {
        $form_state->setErrorByName('max_records', $this->t('Invalid number of records to retain. Specify an integer between 1 and 999.'));
      }
    }

    if (!empty($max_records = $form_state->getValue('max_records'))) {
      $max_records = (int) $max_records;
      if ($max_records < 1 || $max_records > 9999) {
        $form_state->setErrorByName('max_records', $this->t('Invalid number of records to retain. Specify an integer between 1 and 9999.'));
      }
    }

    if (!empty($min_records) && !empty($max_records) && $min_records > $max_records) {
      $form_state->setErrorByName(('max_records'), $this->t('Max records must be equal to or larger than min records'));
    }

    $batch_size = (int) $form_state->getValue('batch_size');
    if ($batch_size < 1 || $batch_size > 1000) {
      $form_state->setErrorByName('batch_size', $this->t('Invalid batch size. Specify an integer between 1 and 1000.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $archive_date = $form_state->getValue('archive_date');
    $min_records = $form_state->getValue('min_records');
    $max_records = $form_state->getValue('max_records');
    $file_format = $form_state->getValue('file_format');
    $batch_size = $form_state->getValue('batch_size');

    $module_path = $this->moduleHandler->getModule('user_history')->getPath();

    $batch = [
      'title' => $this->t('Archiving user history records...'),
      'operations' => [],
      'init_message' => $this->t('Commencing'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing'),
      'finished' => 'user_history_finished_archive_records',
      'file' => $module_path . '/user_history.archive.inc',
    ];

    // Operation to copy the user history records to XML or CSV file.
    $batch['operations'][] = [
      'user_history_archive_records', [
        $archive_date,
        $min_records,
        $max_records,
        $file_format,
        $batch_size,
      ],
    ];

    if ($form_state->getValue('delete_records')) {
      // Operation to delete the archived user history records.
      $batch['operations'][] = [
        'user_history_delete_records', [
          $batch_size,
        ],
      ];
    }

    batch_set($batch);

  }

}
