<?php

namespace Drupal\user_history\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the Batch Restore Form.
 */
class UserHistoryRestoreForm extends FormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Constructs a new UserHistorySettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state api service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, StateInterface $state) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_history_batch_restore';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // If the user history records have not yet been initialised, then re-direct
    // the user to the initialise form instead.
    if ($this->state->get('user_history.initialise_required')) {
      return new RedirectResponse(Url::fromRoute('user_history.batch_install_form')->setAbsolute()->toString());
    }

    $config = $this->configFactory->get('user_history.settings');

    $message_args = [];

    $form['notice'] = [
      '#markup' => '<div>' . $this->t('This process will restore previously archived user history records to the database', $message_args) . '</div>',
    ];

    /** @var \Drupal\Core\StreamWrapper\StreamWrapperManager $streamWrapperManager */
    $streamWrapperManager = \Drupal::service('stream_wrapper_manager');
    // Check if the private file stream wrapper is available for use.
    $filestream = $streamWrapperManager->isValidScheme('private') ? 'private://' : 'public://';
    // Generate a list of archive files that can be restored.
    $directory = $filestream . $config->get('archive.directory');
    $filename = $config->get('archive.filename');
    $pattern = '/' . $filename . '-.*/';
    $files = $this->fileSystem->scanDirectory($directory, $pattern, ['key' => 'uri']);
    foreach ($files as $uri => $file) {
      $files[$uri] = $file->filename;
    }
    // Sort the array of files into reverse order by name.  This should give
    // the list with the newest at the top.
    krsort($files);

    $form['archive_file'] = [
      '#type' => 'select',
      '#title' => $this->t('Archive file'),
      '#description' => $this->t('Select the archive file holding records to be restored.'),
      '#options' => $files,
    ];

    $form['select_uids'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Selected user(s)'),
      '#description' => $this->t('Enter the uid(s) of user accounts to to restore (separate multiple values with commas or spaces).<br />Leave blank for all uids.'),
      '#default_value' => '',
    ];

    $form['batch_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('Enter the number of archived user_history records to process in each batch.'),
      '#default_value' => 100,
      '#required' => TRUE,
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Restore'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $select_uids = $form_state->getValue('select_uids');
    if (!empty($select_uids)) {
      $uids = array_filter(explode(' ', str_replace(',', ' ', $select_uids)));
      foreach ($uids as $uid) {
        if (!is_numeric($uid) || $uid != (int) $uid || $uid < 1) {
          $form_state->setErrorByName('select_uids', $this->t('Invalid uid list. Specify positive integers separated by commas or spaces.'));
          break;
        }
      }
    }

    $size = (int) $form_state->getValue('batch_size');
    if (!is_numeric($size) || $size != (int) $size || $size < 1 || $size > 1000) {
      $form_state->setErrorByName('batch_size', $this->t('Invalid batch size. Specify an integer between 1 and 1000.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $archive_file = $form_state->getValue('archive_file');
    $select_uids = $form_state->getValue('select_uids');
    $uids = empty($select_uids) ? [] : array_filter(explode(' ', str_replace(',', ' ', $select_uids)));
    $batch_size = $form_state->getValue('batch_size');

    $module_path = $this->moduleHandler->getModule('user_history')->getPath();

    $batch = [
      'title' => $this->t('Restoring user history records...'),
      'operations' => [],
      'init_message' => $this->t('Commencing'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing'),
      'finished' => 'user_history_finished_restore_records',
      'file' => $module_path . '/user_history.restore.inc',
    ];

    // Operation to restore the user history records from XML or CSV file.
    $batch['operations'][] = [
      'user_history_restore_records', [
        $archive_file,
        $uids,
        $batch_size,
      ],
    ];

    batch_set($batch);

  }

}
