<?php

namespace Drupal\user_history;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the user_history entity.
 *
 * @see \Drupal\user_history\Entity\UserHistory.
 */
class UserHistoryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\user_history\Entity\UserHistoryInterface $entity */

    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view user_history entities');

      case 'update':
        return AccessResult::forbidden('User history records may not be changed!');

      case 'delete':
        return AccessResult::forbidden('User history records may not be deleted!');
    }

    // Unknown operation, forbid.
    return AccessResult::forbidden('Unknown operation on user history record!');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::forbidden('User history records may not be added manually!');
  }

}
