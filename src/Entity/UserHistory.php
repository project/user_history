<?php

namespace Drupal\user_history\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Defines the user_history entity.
 *
 * While user_history entities are basically content entities, they do not
 * have a published/unpublished status, nor can they be changed once created.
 * There is no provision for adding, editing, or deleting them through the
 * standard entity api.
 *
 * @ingroup user_history
 *
 * @ContentEntityType(
 *   id = "user_history",
 *   label = @Translation("User history"),
 *   handlers = {
 *     "view_builder" = "Drupal\user_history\UserHistoryViewBuilder",
 *     "list_builder" = "Drupal\user_history\UserHistoryListBuilder",
 *     "views_data" = "Drupal\user_history\UserHistoryViewsData",
 *     "access" = "Drupal\user_history\UserHistoryAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\user_history\Form\UserHistoryForm",
 *       "add" = "Drupal\user_history\Form\UserHistoryAddForm",
 *       "edit" = "Drupal\user_history\Form\UserHistoryEditForm",
 *       "delete" = "Drupal\user_history\Form\UserHistoryDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\user_history\UserHistoryHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "user_history",
 *   translatable = FALSE,
 *   admin_permission = "administer user_history entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "canonical" = "/user_history/{user_history}",
 *     "add-form" = "/user_history/add",
 *     "edit-form" = "/user_history/{user_history}/edit",
 *     "delete-form" = "/user_history/{user_history}/delete",
 *     "collection" = "/user_history/list",
 *   },
 *   field_ui_base_route = "user_history.settings"
 * )
 */
class UserHistory extends ContentEntityBase implements UserHistoryInterface {

  /**
   * Sets the label of the user_history entity.
   *
   * @param string $label
   *   The label for the user_history entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setLabel(string $label) {
    $this->set('label', $label);
    return $this;
  }

  /**
   * Gets the creation time of the user_history entity.
   *
   * @return int
   *   Creation timestamp of the user_history entity.
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * Sets the creation time of the user_history entity.
   *
   * @param int $timestamp
   *   The creation timestamp for the user_history entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setCreatedTime(int $timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * Gets the action used to modify the user entity.
   *
   * @return string
   *   Action used to modify the user entity.
   */
  public function getAction() {
    return $this->get('action')->value;
  }

  /**
   * Sets the action used to modify the user entity.
   *
   * @param string $action
   *   The action used to modify the user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setAction(string $action) {
    $this->set('action', $action);
    return $this;
  }

  /**
   * Gets the User account id responsible for modifying the user entity.
   *
   * @return integer
   *   Id of user account responsible for user entity change.
   */
  public function getModifiedByUid() {
    return $this->get('modified_by')->target_id;
  }

  /**
   * Gets the User account responsible for modifying the user entity.
   *
   * @return \Drupal\user\UserInterface
   *   User account responsible for user entity change.
   */
  public function getModifiedBy() {
    return User::load($this->getModifiedByUid());
  }

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account responsible for modifying the user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setModifiedBy(UserInterface $user) {
    $this->set('modified_by', $user->id());
    return $this;
  }

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param int $uid
   *   The id of the account responsible for modifying the user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setModifiedByUid(int $uid) {
    $this->set('modified_by', $uid);
    return $this;
  }

  /**
   * Gets the boolean indicating whether the user entity exists.
   *
   * @return bool
   *   Boolean indicating whether the user entity exists.
   */
  public function getUserDeleted() {
    return $this->get('user_deleted')->value;
  }

  /**
   * Sets the boolean indicating whether the user entity exists.
   *
   * @param bool $deleted
   *   Boolean indicating whether the user entity exists.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserDeleted(bool $deleted) {
    $this->set('user_deleted', $deleted);
    return $this;
  }

  /**
   * Gets the User id from the modified user entity.
   *
   * @return int
   *   user id from the modified user entity.
   */
  public function getUserId() {
    return $this->get('user_id')->value;
  }

  /**
   * Sets the user id from the modified user entity.
   *
   * @param int $uid
   *   The User id from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserid(int $uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * Gets the name from the modified user entity.
   *
   * @return string
   *   The name from the modified user entity.
   */
  public function getUserName() {
    return $this->get('user_name')->value;
  }

  /**
   * Sets the name from the modified user entity.
   *
   * @param string $name
   *   The name from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserName(string $name) {
    $this->set('user_name', $name);
    return $this;
  }

  /**
   * Gets the hashed password from the modified user entity.
   *
   * @return string
   *   The hashed password from the modified user entity.
   */
  public function getUserPass() {
    return $this->get('user_pass')->value;
  }

  /**
   * Sets the hashed password from the modified user entity.
   *
   * @param string $pass
   *   The hashed password from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserPass(string $pass) {
    $this->set('user_pass', $pass);
    return $this;
  }

  /**
   * Gets the mail from the modified user entity.
   *
   * @return string
   *   The mail from the modified user entity.
   */
  public function getUserMail() {
    return $this->get('user_mail')->value;
  }

  /**
   * Sets the mail from the modified user entity.
   *
   * @param string $mail
   *   The mail from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserMail(string $mail) {
    $this->set('user_mail', $mail);
    return $this;
  }

  /**
   * Gets the timezone from the modified user entity.
   *
   * @return string
   *   the timezone from the modified user entity.
   */
  public function getUserTimezone() {
    return $this->get('user_timezone')->value;
  }

  /**
   * Sets the timezone from the modified user entity.
   *
   * @param string $timezone
   *   The timezone from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserTimezone(string $timezone) {
    $this->set('user_timezone', $timezone);
    return $this;
  }

  /**
   * Gets the status from the modified user entity.
   *
   * @return bool
   *   The status from the modified user entity.
   */
  public function getUserStatus() {
    return $this->get('user_status')->value;
  }

  /**
   * Sets the status from the modified user entity.
   *
   * @param bool $status
   *   The status from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserStatus(bool $status) {
    $this->set('user_status', $status);
    return $this;
  }

  /**
   * Gets the roles from the modified user entity.
   *
   * @return string
   *   The "; " delimited roles from the modified user entity.
   */
  public function getUserRoles() {
    return $this->get('user_roles')->value;
  }

  /**
   * Sets the roles from the modified user entity.
   *
   * @param string $roles
   *   The roles from the modified user entity, separated by "; ".
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserRoles(string $roles) {
    $this->set('user_roles', $roles);
    return $this;
  }

  /**
   * Gets the creation timestamp from the modified user entity.
   *
   * @return int
   *   Creation timestamp from the modified user entity.
   */
  public function getUserCreated() {
    return $this->get('user_created')->value;
  }

  /**
   * Sets the creation timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The creation timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserCreated(int $timestamp) {
    $this->set('user_created', $timestamp);
    return $this;
  }

  /**
   * Gets the timestamp of last changed from the modified user entity.
   *
   * @return int
   *   Last changed timestamp from the modified user entity.
   */
  public function getUserChanged() {
    return $this->get('user_changed')->value;
  }

  /**
   * Sets the last changed timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The last changed timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserChanged(int $timestamp) {
    $this->set('user_changed', $timestamp);
    return $this;
  }

  /**
   * Gets the last access timestamp from the modified user entity.
   *
   * @return int
   *   last access timestamp from the modified user entity.
   */
  public function getUserAccess() {
    return $this->get('user_access')->value;
  }

  /**
   * Sets the last access timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The last access timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserAccess(int $timestamp) {
    $this->set('user_access', $timestamp);
    return $this;
  }

  /**
   * Gets the most recent login timestamp from the modified user entity.
   *
   * @return int
   *   Most recent login timestamp from the modified user entity.
   */
  public function getUserLogin() {
    return $this->get('user_login')->value;
  }

  /**
   * Sets the most recent login timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The most recent login timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserLogin(int $timestamp) {
    $this->set('user_login', $timestamp);
    return $this;
  }

  /**
   * Gets the original email from the modified user entity.
   *
   * @return int
   *   The original email from the modified user entity.
   */
  public function getUserInit() {
    return $this->get('user_init')->value;
  }

  /**
   * Sets the original email from the modified user entity.
   *
   * @param string $mail
   *   The original from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserInit(string $mail) {
    $this->set('user_init', $mail);
    return $this;
  }

  /**
   * Gets the user langcode from the modified user entity.
   *
   * @return string
   *   User langcode from the modified user entity.
   */
  public function getUserlangcode() {
    return $this->get('user_langcode')->value;
  }

  /**
   * Sets the user langcode from the modified user entity.
   *
   * @param string $langcode
   *   The user langcode from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserLangcode(string $langcode) {
    $this->set('user_langcode', $langcode);
    return $this;
  }

  /**
   * Gets the user preferred langcode from the modified user entity.
   *
   * @return string
   *   User preferred langcode from the modified user entity.
   */
  public function getUserPreferredLangcode() {
    return $this->get('user_preferred_langcode')->value;
  }

  /**
   * Sets the user preferred langcode from the modified user entity.
   *
   * @param string $langcode
   *   The user preferred langcode from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserPreferredLangcode(string $langcode) {
    $this->set('user_preferred_langcode', $langcode);
    return $this;
  }

  /**
   * Gets the user preferred admin langcode from the modified user entity.
   *
   * @return string
   *   User preferred admin langcode from the modified user entity.
   */
  public function getUserPreferredAdminLangcode() {
    return $this->get('user_preferred_admin_langcode')->value;
  }

  /**
   * Sets the user preferred admin langcode from the modified user entity.
   *
   * @param string $langcode
   *   The user preferred admin langcode from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserPreferredAdminLangcode(string $langcode) {
    $this->set('user_preferred_admin_langcode', $langcode);
    return $this;
  }

  /**
   * Gets the difference between the modified user entity and a previous copy.
   *
   * @return string
   *   The difference between the modified user entity and a previous copy.
   */
  public function getDifference() {
    return $this->get('difference')->value;
  }

  /**
   * Sets the difference between the modified user entity and a previous copy.
   *
   * @param string $difference
   *   The differences between the modified user entity and a previous copy.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setDifference(string $difference) {
    if (strlen($difference) > 255) {
      $difference = substr($difference, 0, 252) . '...';
    }
    $this->set('difference', $difference);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // Start with the base fields for a content entity type.
    // This includes only id for user_history entities as specified
    // in the annotation above.
    $fields = parent::baseFieldDefinitions($entity_type);

    $weight = 0;

    // Define some fields specific to this entity type.
    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('Auto-create label for the user_history entity'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Modified on'))
      ->setDescription(t('The time that the user_history entity was created.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => $weight++,
      ]);

    $fields['action'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Action'))
      ->setDescription(t('The action used to modify the user entity.'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['modified_by'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Modified by'))
      ->setDescription(t('The account making the changes to the user entity.'))
      ->setSetting('target_type', 'user')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => $weight++,
      ]);

    $fields['user_deleted'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('User deleted'))
      ->setDescription(t('A boolean indicating whether the user entity was deleted.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'boolean',
        'settings' => [
          'format' => 'yes-no',
        ],
        'weight' => $weight++,
      ]);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The ID of the modified account.'))
      ->setSetting('target_type', 'user')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_entity_id',
        'weight' => $weight++,
      ]);

    $fields['user_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the modified account.'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['user_pass'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Password'))
      ->setDescription(t('The password of this user (hashed).'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['user_mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription(t('The email of the modified account.'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['user_timezone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Timezone'))
      ->setDescription(t('The timezone of the modified account.'))
      ->setSetting('max_length', 32)
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['user_status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('User status'))
      ->setDescription(t('Whether the user is active or blocked.'))
      ->setDefaultValue(FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'boolean',
        'settings' => [
          'format' => 'custom',
          'format_custom_true' => 'Active',
          'format_custom_false' => 'Blocked',
        ],
        'weight' => $weight++,
      ]);

    $fields['user_roles'] = BaseFieldDefinition::create('string')
      ->setLabel(t('User roles'))
      ->setDescription(t('The roles the user has.'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['user_created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('User created'))
      ->setDescription(t('The time that the modified account was created.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => $weight++,
      ]);

    $fields['user_changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('User changed'))
      ->setDescription(t('The time that the modified account was last edited.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => $weight++,
      ]);

    $fields['user_access'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last access'))
      ->setDescription(t('The time that the modified account last accessed the site.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => $weight++,
      ]);

    $fields['user_login'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last login'))
      ->setDescription(t('The time that the modified account last logged in.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => $weight++,
      ]);

    $fields['user_init'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Initial email'))
      ->setDescription(t('The email address used for initial account creation.'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight++,
      ]);

    $fields['user_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The modified account language code.'))
      ->setDefaultValue([0 => ['value' => NULL]])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'language',
        'weight' => $weight++,
      ]);

    $fields['user_preferred_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Preferred language code'))
      ->setDescription(t("The modified account's preferred language code for receiving emails and viewing the site."))
      ->setDefaultValue([0 => ['value' => NULL]])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'language',
        'weight' => $weight++,
      ]);

    $fields['user_preferred_admin_langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Preferred admin language code'))
      ->setDescription(t("The modified account's preferred language code for viewing administration pages."))
      ->setDefaultValue([0 => ['value' => NULL]])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'language',
        'weight' => $weight++,
      ]);

    $fields['difference'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Difference'))
      ->setDescription(t('The difference between this user_history record and the previous one for same user account'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => $weight,
      ]);

    return $fields;
  }

  /**
   * Return a default value for the standard entity function "isPublished".
   *
   * @return bool
   *   Value indicating the user_history entity is published.
   */
  public function isPublished() {
    return TRUE;
  }

  /**
   * Return the id of the default (and only) bundle.
   *
   * @return string
   *   The bundle id.
   */
  public function bundle() {
    return 'user_history';
  }

}
