<?php

namespace Drupal\user_history\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining user_history entities.
 *
 * @ingroup user_history
 */
interface UserHistoryInterface extends ContentEntityInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Sets the label of the user_history entity.
   *
   * @param string $label
   *   The label for the user_history entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setLabel(string $label);

  /**
   * Gets the User history creation timestamp.
   *
   * @return int
   *   Creation timestamp of the User history.
   */
  public function getCreatedTime();

  /**
   * Sets the User history creation timestamp.
   *
   * @param int $timestamp
   *   The User history creation timestamp.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setCreatedTime(int $timestamp);

  /**
   * Gets the action used to modify the user entity.
   *
   * @return string
   *   Action used to modify the user entity.
   */
  public function getAction();

  /**
   * Sets the action used to modify the user entity.
   *
   * @param string $action
   *   The action used to modify the user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setAction(string $action);

  /**
   * Gets the User account id responsible for modifying the user entity.
   *
   * @return integer
   *   Id of user account responsible for user entity changes.
   */
  public function getModifiedByUid();

  /**
   * Gets the User account responsible for modifying the user entity.
   *
   * @return \Drupal\user\UserInterface
   *   User account responsible for user entity changes.
   */
  public function getModifiedBy();

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account responsible for modifying the user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setModifiedBy(UserInterface $user);

  /**
   * Sets the user account responsible for modifying the user entity.
   *
   * @param int $uid
   *   The id of the account responsible for modifying the user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setModifiedByUid(int $uid);

  /**
   * Gets the boolean indicating whether the user entity has been deleted.
   *
   * @return bool
   *   Boolean indicating whether the user entity deleted.
   */
  public function getUserDeleted();

  /**
   * Sets the boolean indicating whether the user entity has been deleted.
   *
   * @param bool $deleted
   *   Boolean indicating whether the user entity deleted.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserDeleted(bool $deleted);

  /**
   * Gets the User id from the modified user entity.
   *
   * @return int
   *   user id from the modified user entity.
   */
  public function getUserId();

  /**
   * Sets the user id from the modified user entity.
   *
   * @param int $uid
   *   The User id from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserid(int $uid);

  /**
   * Gets the name from the modified user entity.
   *
   * @return string
   *   The name from the modified user entity.
   */
  public function getUserName();

  /**
   * Sets the name from the modified user entity.
   *
   * @param string $name
   *   The name from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserName(string $name);

  /**
   * Gets the hashed password from the modified user entity.
   *
   * @return string
   *   The hashed password from the modified user entity.
   */
  public function getUserPass();

  /**
   * Sets the hashed password from the modified user entity.
   *
   * @param string $pass
   *   The hashed password from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserPass(string $pass);

  /**
   * Gets the mail from the modified user entity.
   *
   * @return string
   *   The mail from the modified user entity.
   */
  public function getUserMail();

  /**
   * Sets the mail from the modified user entity.
   *
   * @param string $mail
   *   The mail from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserMail(string $mail);

  /**
   * Gets the timezone from the modified user entity.
   *
   * @return string
   *   the timezone from the modified user entity.
   */
  public function getUserTimezone();

  /**
   * Sets the timezone from the modified user entity.
   *
   * @param string $timezone
   *   The timezone from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserTimezone(string $timezone);

  /**
   * Gets the status from the modified user entity.
   *
   * @return bool
   *   The status from the modified user entity.
   */
  public function getUserStatus();

  /**
   * Sets the status from the modified user entity.
   *
   * @param bool $status
   *   The status from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserStatus(bool $status);

  /**
   * Gets the roles from the modified user entity.
   *
   * @return string
   *   The "; " delimited roles from the modified user entity.
   */
  public function getUserRoles();

  /**
   * Sets the roles from the modified user entity.
   *
   * @param string $roles
   *   The roles from the modified user entity, separated by "; ".
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserRoles(string $roles);

  /**
   * Gets the creation timestamp from the modified user entity.
   *
   * @return int
   *   Creation timestamp from the modified user entity.
   */
  public function getUserCreated();

  /**
   * Sets the creation timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The creation timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserCreated(int $timestamp);

  /**
   * Gets the timestamp of last changed from the modified user entity.
   *
   * @return int
   *   Last changed timestamp from the modified user entity.
   */
  public function getUserChanged();

  /**
   * Sets the last changed timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The last changed timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserChanged(int $timestamp);

  /**
   * Gets the last access timestamp from the modified user entity.
   *
   * @return int
   *   last access timestamp from the modified user entity.
   */
  public function getUserAccess();

  /**
   * Sets the last access timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The last access timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserAccess(int $timestamp);

  /**
   * Gets the most recent login timestamp from the modified user entity.
   *
   * @return int
   *   Most recent login timestamp from the modified user entity.
   */
  public function getUserLogin();

  /**
   * Sets the most recent login timestamp from the modified user entity.
   *
   * @param int $timestamp
   *   The most recent login timestamp from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserLogin(int $timestamp);

  /**
   * Gets the original email from the modified user entity.
   *
   * @return int
   *   The original email from the modified user entity.
   */
  public function getUserInit();

  /**
   * Sets the original email from the modified user entity.
   *
   * @param string $mail
   *   The original from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserInit(string $mail);

  /**
   * Gets the user langcode from the modified user entity.
   *
   * @return string
   *   User langcode from the modified user entity.
   */
  public function getUserlangcode();

  /**
   * Sets the user langcode from the modified user entity.
   *
   * @param string $langcode
   *   The user langcode from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserLangcode(string $langcode);

  /**
   * Gets the user preferred langcode from the modified user entity.
   *
   * @return string
   *   User preferred langcode from the modified user entity.
   */
  public function getUserPreferredLangcode();

  /**
   * Sets the user preferred langcode from the modified user entity.
   *
   * @param string $langcode
   *   The user preferred langcode from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserPreferredLangcode(string $langcode);

  /**
   * Gets the user preferred admin langcode from the modified user entity.
   *
   * @return string
   *   User preferred admin langcode from the modified user entity.
   */
  public function getUserPreferredAdminLangcode();

  /**
   * Sets the user preferred admin langcode from the modified user entity.
   *
   * @param string $langcode
   *   The user preferred admin langcode from the modified user entity.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setUserPreferredAdminLangcode(string $langcode);

  /**
   * Gets the difference between the modified user entity and a previous copy.
   *
   * @return string
   *   the difference between the modified user entity and a previous copy.
   */
  public function getDifference();

  /**
   * Sets the user preferred admin langcode from the modified user entity.
   *
   * @param string $difference
   *   The differences between the modified user entity and a previous copy.
   *
   * @return \Drupal\user_history\Entity\UserHistoryInterface
   *   The called user_history entity.
   */
  public function setDifference(string $difference);

}
