<?php

namespace Drupal\user_history;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a standard entity view builder.
 *
 * Override default EntityViewBuilder methods here.
 */
class UserHistoryViewBuilder extends EntityViewBuilder {

}
