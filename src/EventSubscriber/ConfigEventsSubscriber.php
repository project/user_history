<?php

namespace Drupal\user_history\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\State\StateInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\user_history\EventSubscriber
 */
class ConfigEventsSubscriber implements EventSubscriberInterface {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Constructs a new ConfigEventsSubscriber.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state api service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE => 'configSave',
      ConfigEvents::DELETE => 'configDelete',
    ];
  }

  /**
   * React to a config object being saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function configSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    if ($config->getName() == 'user_history.settings') {
      // Check whether the initialise process has run.
      $initialised = !$this->state->get('user_history.initialise_required');

      // Get the current running config and compare with the updated config.
      if ($initialised) {
        $current_base_fields = $this->state->get('user_history.base_fields');
        if ($config->get('base_fields') != $current_base_fields) {
          // Set a flag to remind that the batch field update needs to be run.
          $this->state->set('user_history.base_fields_update_required', TRUE);
        }
        else {
          // Clear flag indicating that the batch field update needs to be run.
          $this->state->set('user_history.base_fields_update_required', FALSE);
        }
        $current_attached_fields = $this->state->get('user_history.attached_fields');
        if ($config->get('attached_fields') != $current_attached_fields) {
          // Set a flag to remind that the batch field update needs to be run.
          $this->state->set('user_history.attached_fields_update_required', TRUE);
        }
        else {
          // Clear flag indicating that the batch field update needs to be run.
          $this->state->set('user_history.attached_fields_update_required', FALSE);
        }
      }
      else {
        // Set a flag to remind that the batch initialisation needs to be run.
        \Drupal::state()->set('user_history.initialise_required', TRUE);
      }

    }

  }

  /**
   * React to a config object being deleted.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   *
   * @noinspection PhpStatementHasEmptyBodyInspection
   */
  public function configDelete(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    if ($config->getName() == 'user_history.settings') {
      // The user_history module is probably being uninstalled.  Actions are in
      // hook_uninstall().
    }
  }

}
