<?php

namespace Drupal\user_history;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for user_history entities.
 */
class UserHistoryViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   * @noinspection PhpUnnecessaryLocalVariableInspection
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins,
    // can be put here.
    return $data;
  }

}
