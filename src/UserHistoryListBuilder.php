<?php

namespace Drupal\user_history;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of user_history entities.
 *
 * @ingroup user_history
 */
class UserHistoryListBuilder extends EntityListBuilder {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new UserHistoryListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type, $entity_type_manager->getStorage($entity_type->id()));
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['user_name'] = $this->t('User name');
    $header['user_mail'] = $this->t('User mail');
    $header['modified_by'] = $this->t('Modified by');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\user_history\Entity\UserHistory $entity */
    $row['label'] = Link::createFromRoute(
      $entity->label(),
      'entity.user_history.canonical',
      ['user_history' => $entity->id()]
    );
    $row['user_name'] = $entity->getUserName();
    $row['user_mail'] = $entity->getUserMail();
    /** @var \Drupal\user\Entity\User $modified_by */
    $modified_by = $this->entityTypeManager
      ->getStorage('user')
      ->load($entity->getModifiedBy());
    $row['modified_by'] = $modified_by->getDisplayName() . ' (' . $modified_by->id() . ')';
    return $row + parent::buildRow($entity);
  }

}
