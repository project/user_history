<?php

/**
 * @file
 * Contains functions for archiving user_history records.
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\File\FileSystemInterface;
use Drupal\user_history\Entity\UserHistoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Function to archive user_history records.
 *
 * This function is executed in a batch context.
 *
 * @param string $archive_date
 *   The date for which equal and older records will be archived.
 * @param int $min_records
 *   The minimum number of user_history records to retain for a user account.
 * @param int $max_records
 *   The maximum number of user_history records to retain for a user account.
 * @param string $file_format
 *   The required file format ("txt", "csv", etc).
 * @param int $batch_size
 *   The number of user accounts to process in each batch run.
 * @param array $context
 *   The batch context array.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function user_history_archive_records(string $archive_date, int $min_records, int $max_records, string $file_format, int $batch_size, array &$context) {

  $config = \Drupal::config('user_history.settings');

  // Convert the $archive_date array to a timestamp.
  if (!empty($archive_date)) {
    $archive_date = DrupalDateTime::createFromFormat('Y-m-d', $archive_date);
    $archive_timestamp = $archive_date->format('U');
  }
  else {
    $archive_timestamp = 0;
  }

  // Initialise the sandbox values on the first call.
  // The batch process will iterate through all user ids.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = \Drupal::entityQuery('user')
      ->condition('uid', 0, '>')
      ->count()
      ->accessCheck(FALSE)
      ->execute();

    /** @var \Drupal\Core\StreamWrapper\StreamWrapperManager $streamWrapperManager */
    $streamWrapperManager = \Drupal::service('stream_wrapper_manager');
    // Check if the private file stream wrapper is available for use.
    $filepath = $streamWrapperManager->isValidScheme('private') ? 'private://' : 'public://';
    // Append the specified archive file path to the file scheme.
    $filepath .= $config->get('archive.directory') ?? 'user_history/archive/';
    // Get the file name and append the current date/time.
    $filename = $config->get('archive.filename') ?? 'user-history-archive';
    $filename .= '-' . date('Ymd') . '-' . date('Hi') . '.' . $file_format;

    // Ensure the filepath exists.
    if (!\Drupal::service('file_system')->prepareDirectory($filepath, FileSystemInterface::CREATE_DIRECTORY)) {
      $context['message'] = t('ERROR - Unable to access or create the specified archive directory');
      $context['results']['total'] = 0;
      $context['results']['archived'] = 0;
      $context['finished'] = 1;
      return;
    }

    $context['sandbox']['last'] = 0;
    $context['sandbox']['count'] = 0;
    $context['sandbox']['archived'] = 0;
    $context['sandbox']['ids'] = [];
    $context['sandbox']['filepath'] = $filepath;
    $context['sandbox']['filename'] = $filename;
    $context['sandbox']['header_write'] = FALSE;

    $header_write = FALSE;

    // Initialise the output file.
    $archive_file = fopen($filepath . $filename, "w");
  }
  else {
    // Recover the values of variables from the context sandbox.
    $filepath = $context['sandbox']['filepath'];
    $filename = $context['sandbox']['filename'];
    $header_write = $context['sandbox']['header_write'];

    // Re-open the output file for subsequent batches.
    $archive_file = fopen($filepath . $filename, "a");
  }

  // Retrieve the next set of user ids.
  $uids = \Drupal::entityQuery('user')
    ->condition('uid', $context['sandbox']['last'], '>')
    ->sort('uid')
    ->range(0, $batch_size)
    ->accessCheck(FALSE)
    ->execute();

  foreach ($uids as $uid) {

    // Count the user_history records for this account.
    $num_records = \Drupal::entityQuery('user_history')
      ->condition('user_id', $uid)
      ->count()
      ->accessCheck(FALSE)
      ->execute();

    // Ensure that there are at least $min_records history records for this
    // account before considering any archiving.
    if ($num_records > $min_records) {
      $uhids = [];
      // Find records that are earlier then the archive date.
      $ids = \Drupal::entityQuery('user_history')
        ->condition('user_id', $uid)
        ->condition('created', $archive_timestamp, '<')
        ->sort('created')
        ->accessCheck(FALSE)
        ->execute();
      if (count($ids) > 0) {
        // Check that we are not archiving / deleting too many records.
        $remaining = $num_records - count($ids);
        while ($remaining < $min_records) {
          // Want to keep the most recent $min_records.
          array_pop($ids);
          $remaining++;
        }
        $uhids = $ids;
      }
      // Find old records that are in excess of the max retained value.
      if ($num_records > $max_records) {
        $ids = \Drupal::entityQuery('user_history')
          ->condition('user_id', $uid)
          ->sort('created')
          ->range(0, $num_records - $max_records)
          ->accessCheck(FALSE)
          ->execute();
        $uhids = array_unique($uhids + $ids);
      }

      // Now load and archive the selected records.
      /** @var \Drupal\user_history\Entity\userHistory[] $user_histories */
      $user_histories = \Drupal::entityTypeManager()->getStorage('user_history')->loadMultiple($uhids);

      foreach ($user_histories as $user_history) {

        $uh_field_info = user_history_get_user_history_fields();

        switch ($file_format) {
          case 'txt':
            $record = user_history_values_to_txt($user_history) . PHP_EOL;
            break;

          case 'csv':
            $max_cardinality = $config->get('archive.max_cardinality');
            if (!$header_write) {
              $headers = user_history_csv_headers($uh_field_info, $max_cardinality);
              fwrite($archive_file, implode(',', $headers) . PHP_EOL);
              $header_write = TRUE;
            }
            $record = user_history_values_to_csv($user_history, $uh_field_info, $max_cardinality) . PHP_EOL;
            break;

          case 'xml':
            if (!$header_write) {
              $headers = '<?xml version="1.0"?><user_histories>';
              fwrite($archive_file, $headers . PHP_EOL);
              $header_write = TRUE;
            }
            $record = user_history_values_to_xml($user_history) . PHP_EOL;
            break;

          case 'json':
            $record = user_history_values_to_json($user_history) . PHP_EOL;
            break;

          default:
            $record = NULL;
        }
        fwrite($archive_file, $record);
        // Update count of archived user history records.
        $context['sandbox']['archived']++;
        // Save the id of the archived user_history record.
        $context['sandbox']['ids'][] = $user_history->id();
      }

      // Have processed all the user history records for this account.
    }
    // Update count of uids processed.
    $context['sandbox']['count']++;
    $context['sandbox']['last'] = $uid;
  }

  // Finished this batch of uids, so close archive file.
  fclose($archive_file);
  // Save header_write state.
  $context['sandbox']['header_write'] = $header_write;

  // Check if all uids have been processed or whether there are more batches to
  // process.
  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user accounts.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;

    // Add closing tag to XML file if necessary.
    if ($file_format == 'xml') {
      $archive_file = fopen($filepath . $filename, "a");
      fwrite($archive_file, '</user_histories>');
      fclose($archive_file);
    }

    $context['message'] = t('Finished processing %total user accounts. Archived %num user history records', [
      '%total' => $context['sandbox']['total'],
      '%num' => $context['sandbox']['archived'],
    ]);
    $context['results']['total'] = $context['sandbox']['total'];
    $context['results']['archived'] = $context['sandbox']['archived'];
    $context['results']['ids'] = $context['sandbox']['ids'];
  }
}

/**
 * Delete archived user_history records.
 *
 * This function is executed in a batch context.
 *
 * @param int $batch_size
 *   The number of user user_history records to delete in each batch run.
 * @param array $context
 *   The batch context array.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function user_history_delete_records(int $batch_size, array &$context) {

  // Initialise the sandbox values on the first call.
  // The user_history record ids to delete are in $context['results'].
  // The batch process will process the set of archived user_history ids.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['total'] = 0;
    $context['sandbox']['count'] = 0;
    $context['sandbox']['deleted'] = 0;
  }

  if (isset($context['results']['ids']) && is_array($context['results']['ids'])) {
    // Set the total number of records to delete.
    if ($context['sandbox']['total'] == 0) {
      $context['sandbox']['total'] = count($context['results']['ids']);
    }

    // Extract the next set of user_history ids.
    $uhids = array_slice($context['results']['ids'], $context['sandbox']['count'], $batch_size);

    // Now load and delete the selected records.
    $user_histories = \Drupal::entityTypeManager()->getStorage('user_history')->loadMultiple($uhids);

    foreach ($user_histories as $user_history) {
      $user_history->delete();
      // Update count of deleted user history records.
      $context['sandbox']['deleted']++;
    }

    // Update count of user_history ids processed.
    $context['sandbox']['count'] = $context['sandbox']['count'] + count($uhids);

  }
  // Check if all ids have been processed or whether there are more batches to
  // process.
  if ($context['sandbox']['count'] < $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
    if ($context['finished'] > 0.99) {
      $context['finished'] = 0.99;
    }
    $context['message'] = t('Processed %count of %total user_history records.', [
      '%count' => $context['sandbox']['count'],
      '%total' => $context['sandbox']['total'],
    ]);
  }
  else {
    $context['finished'] = 1;
    $context['message'] = t('Finished processing %total user_history records. Deleted %num user history records', [
      '%total' => $context['sandbox']['total'],
      '%num' => $context['sandbox']['deleted'],
    ]);
    $context['results']['total2'] = $context['sandbox']['total'];
    $context['results']['deleted'] = $context['sandbox']['deleted'];
  }
}

/**
 * Batch processing finish callback.
 *
 * Function to handle completion of batch process to archive and optionally
 * delete user history records.  Display a message to user and write to log.
 *
 * @param bool $success
 *   The success status of the batch operation.
 * @param array $results
 *   The set of ids that have been successfully processed.
 * @param array $operations
 *   The list of operations that have been performed.
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 *   The next page to display to the user.
 */
function user_history_finished_archive_records(bool $success, array $results, array $operations) {

  if ($success) {
    // Display a completion message to the user.
    $message = t('Processed %total user accounts and archived %num user history records', [
      '%total' => $results['total'],
      '%num' => $results['archived'],
    ]);

    \Drupal::messenger()->addMessage($message);
    \Drupal::logger('user_history')->notice($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing @operation with arguments : @args', [
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    ]);

    \Drupal::messenger()->addMessage($message);
    \Drupal::logger('user_history')->error($message);
  }

  // Redirect to the system status report page.
  return new RedirectResponse(Url::fromRoute('system.status')->setAbsolute()->toString());
}

/**
 * Return an array of values that can be used as a CSV header record.
 *
 * Required for a CSV archive file.
 *
 * @param array $uh_field_info
 *   The array of field information arrays.
 * @param int $max_cardinality
 *   The maximum cardinality of a multi-value field to allow separate CSV cols,
 *   else the values will be shown as a serialized array.
 *
 * @return array
 *   The array of CSV header labels.
 */
function user_history_csv_headers(array $uh_field_info, int $max_cardinality) {

  $headers = [];

  foreach ($uh_field_info as $field_name => $field_info) {
    $cardinality = $field_info['cardinality'];
    unset($field_info['cardinality']);
    if ($cardinality == -1 || $cardinality > $max_cardinality) {
      $headers[] = $field_name . ':serialized';
    }
    elseif ($cardinality == 1) {
      foreach ($field_info as $property) {
        $headers[] = $field_name . ':' . $property;
      }
    }
    else {
      for ($count = 0; $count < $cardinality; $count++) {
        foreach ($field_info as $property) {
          $headers[] = $field_name . ':' . $count . ':' . $property;
        }
      }
    }
  }
  return $headers;
}

/**
 * Convert a user_history entity to a txt string.
 *
 * Function to convert a user_history entity to a string for adding to a TXT
 * archive file.  The user_history entity is first converted to an array and
 * then the array is serialized and massaged to deal with embedded newline
 * characters.
 *
 * @param \Drupal\user_history\Entity\UserHistoryInterface $user_history
 *   The user_history record to convert.
 *
 * @return string
 *   The encoded user_history record values.
 */
function user_history_values_to_txt(UserHistoryInterface $user_history) {
  // Convert user_history entity to an array of values.
  $uh_values = $user_history->toArray();
  // Generate a serialised array, and encode CR/LF chars.
  return str_replace(["\n", "\r"], ['\n', '\r'], serialize($uh_values));
}

/**
 * Convert a user_history record into a CSV row.
 *
 * Function to convert a user_history entity into a string for adding to a CSV
 * archive file. The user_history entity is first converted to an array and
 * then the elements of the array are processed to generate CSV columns matching
 * the csv header that has been generated.
 *
 * @param \Drupal\user_history\Entity\UserHistoryInterface $user_history
 *   The user_history record to convert.
 * @param array $uh_field_info
 *   The array of field information arrays.
 * @param int $max_cardinality
 *   The maximum cardinality of a multi-value field to allow separate CSV cols,
 *   else the values will be shown as a serialized array.
 *
 * @return string
 *   The user_history record values encoded as a CSV row.
 */
function user_history_values_to_csv(UserHistoryInterface $user_history, array $uh_field_info, int $max_cardinality) {
  // Convert user_history entity to an array of values.
  $uh_values = $user_history->toArray();

  $return = [];
  foreach ($uh_field_info as $field_name => $field_info) {
    $cardinality = $field_info['cardinality'];
    unset($field_info['cardinality']);
    $field_values = $uh_values[$field_name];
    if ($cardinality == -1 || $cardinality > $max_cardinality) {
      $return[] = serialize($field_values);
    }
    else {
      for ($count = 0; $count < $cardinality; $count++) {
        if (isset($field_values[$count])) {
          foreach ($field_info as $property) {
            $return[] = $field_values[$count][$property];
          }
        }
        else {
          // No values for this delta of the field.
          /** @noinspection PhpUnusedLocalVariableInspection */
          foreach ($field_info as $property) {
            // $property is unused in this branch.
            // Present for development/debug purposes.
            $return[] = NULL;
          }
        }
      }
    }
  }

  // Add quotes to any string values and escape commas and quote marks.
  foreach ($return as $key => $value) {
    if (!is_numeric($value) && !is_null($value)) {
      $return[$key] = '"' . str_replace([',', '"'], ['\,', '\"'], trim($value, '"')) . '"';
    }
  }

  // Encode any CR or LF chars present in the return value.
  return str_replace(["\n", "\r"], ['\n', '\r'], implode(',', $return));

}

/**
 * Convert a user_history record to an XML data structure.
 *
 * Function to convert a user_history entity into a serialized XML data
 * structure using the core normalizations and serialization functions.
 *
 * @param \Drupal\user_history\Entity\UserHistoryInterface $user_history
 *   The user_history record to convert.
 *
 * @return string
 *   The user history record values encoded as an XML data structure.
 */
function user_history_values_to_xml(UserHistoryInterface $user_history) {

  $record = \Drupal::service('serializer')->serialize($user_history, 'xml');

  // Strip the xml tag from the front of the record and trim any CR or LF
  // characters.
  $record = trim(preg_replace('/^<\?xml(.*)\?>/', '', $record));

  // Encode any CR and LF chars in the record.
  $record = str_replace(["\n", "\r"], ['\n', '\r'], $record);

  // Replace the outer Xml tag with "<user_history> </user_history>".
  return preg_replace('#^<(.*?)>(.*)</(.*?)>$#', "<user_history>$2</user_history>", $record);

}

/**
 * Convert a user_history record to a JSON data structure.
 *
 * Function to convert a user_history entity into a serialized JSON data
 * structure using the core normalizations and serialization functions.
 *
 * @param \Drupal\user_history\Entity\UserHistoryInterface $user_history
 *   The user_history record to convert.
 *
 * @return string
 *   The user_history record values encoded as a JSON data structure.
 */
function user_history_values_to_json(UserHistoryInterface $user_history) {

  return \Drupal::service('serializer')->serialize($user_history, 'json');

}

/**
 * Return filed information for user_history record.
 *
 * Function to get the user_history fields and return an array specifying the
 * properties and cardinality of each field.
 *
 * @return array
 *   An array of field info to the user_history record fields.
 */
function user_history_get_user_history_fields() {
  $return = [];
  $entity_field_manager = \Drupal::service('entity_field.manager');
  $field_definitions = $entity_field_manager->getFieldDefinitions('user_history', 'user_history');
  foreach ($field_definitions as $field_name => $field_definition) {

    if ($field_definition instanceof BaseFieldDefinition) {
      $return[$field_name][] = $field_definition->getMainPropertyName();
      $return[$field_name]['cardinality'] = $field_definition->getCardinality();
    }
    else {
      // Field added through UI or programmatically.
      /** @var \Drupal\field\Entity\FieldConfig $field_config */
      $field_config = $field_definition;
      $field_storage_defn = $field_config->getFieldStorageDefinition();
      foreach (array_keys($field_storage_defn->getColumns()) as $property) {
        $return[$field_name][] = $property;
      }
      $return[$field_name]['cardinality'] = $field_storage_defn->getCardinality();
    }
  }
  return $return;
}
