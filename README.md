CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* FAQ
* Maintainers


INTRODUCTION
------------

This module will provide functionality to track and report on changes to user
entities. The user entity changes are recorded in the database as a
user_history entity that records the values of the user entity properties
after any change (insert or update). If a user entity is deleted, then a
history record is saved recording the deletion and the final values of the user
entity properties.


REQUIREMENTS
------------

This module requires only the core user module.


INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

2. Run the batch initialise process to create initial values for all existing
   user accounts. Go to "/user_history/initialise" or visit the status report
   and click on the link.


CONFIGURATION
-------------

The configuration for this module is made in:
* Administration | Configuration | People | User history | User history settings

There are four groups of settings:

1. No change
   These settings are for controlling how user_history records that represent no
   change from the previous state are handled.
   * Ignore - checking this box this will cause the system to ignore all such
              records
   * Delete - specifying a time interval here (e.g., "1 week") will cause the
              system to record the records, but to delete them once the time
              interval has passed. Deletion is via one or more batch jobs
              created via a cron task.
   * Batch size - this controls the number of aged records to be deleted in
              each batch job started by cron.

2. Base fields
   These settings are selecting the user account base fields that will be
   tracked by the module.  They are all selected by default, and there is no
   facility to change that selection at present.

3. Attached fields
   These settings allow the fields that have been attached to the user account
   via the field UI, or programmatically, to be selected for tracking. Each
   field selected here will cause an appropriate field to be attached to
   user_history entity via the field API.

4. Archive settings
   These settings allow user history records to be archived, and optionally
   removed from the database to prevent the tables from growing too large.
   * Directory - specify the directory in the "private://" file system (if it
     exists, else the "public://" file system will be used).
   * File name - Base name for the archive files (e.g., "user-history-archive").
     A date-time string will be appended to this base to give the full name.
   * Max cardinality - specify the maximum cardinality of a multi-valued field
     for the property values to have separate columns in a CSV archive. Fields
     with greater cardinality will have the property values structured into an
     array which is then serialised before being added to the archive file
     record.


FAQ
---

The initial release will only record changes to the default properties of a
user entity. These are:
1. 'User id' => 'uid',
2. 'User name' => 'name',
3. 'User password' => 'pass',
4. 'User email' => 'mail',
5. 'User timezone' => 'timezone',
6. 'User status' => 'status',
7. 'User roles' => 'roles',
8. 'User created' => 'created',
9. 'User changed' => 'changed',
10. 'User access' => 'access',
11. 'user login' => 'login',
12. 'User init' => 'init',
13. 'User langcode' => 'langcode',
14. 'User preferred langcode' => 'preferred_langcode',
15. 'User preferred admin langcode' => 'preferred_admin_langcode',

User roles are obtained from the user account and recorded as an imploded
string with "; " separator.

The properties "changed", "access", "login" are not particularly useful in
tracking changes to a user account as these values are updated automatically by
Drupal during its normal operation. This module is intended to track changes
that are the result of an operator action.

The user history entities can be processed using the standard entity API
functions, except that there is no ability to:
  * manually add a new record,
  * update an existing record,
  * delete one from the table.

Initial values for each existing user account are generated via a batch process
after the module has been installed.  Running this batch process is currently
a manual task, but this may be changed to be automatic at some point in the
future.

The user_history entities can be viewed using different view modes.  Two are
provided by default
- a default mode which displays the full record
- a tab mode for display of key information from each change on a new tab of
  the user account page.

The user_history entities are fully compatible with views.  There are two
default views provided with the module:
- a list of all user history records that can be filtered for records:
    * affecting a specified user account,
    * for changes within a date range,
    * for changes involving a specified role,
    * for changes made by a specified user.
- a list of changes (rendered using the tab view mode) for a specified user
  account to be displayed on a new tab on the user account page.

User_history entities are rendered using the user_history twig template
provided with the module, with a small CSS file attached to ensure a consistent
basic layout.  The template and attached CSS file can be overridden by adding
to a custom theme and modifying if required.

The module will keep any existing records for a user account which is deleted,
and will add a new record to record the deletion to ensure the history is
retained.


MAINTAINERS
-----------

Current maintainers:
* James Scott (jlscott) - https://www.drupal.org/user/213325

This project has been sponsored by:
* XEQUALS - https://xequals.nz/
