<?php

/**
 * @file
 * Contains user_history.page.inc.
 *
 * Page callback for user_history entities.
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for User history templates.
 *
 * Default template: user_history.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 *
 * @throws \Drupal\Core\Entity\EntityMalformedException
 */
function template_preprocess_user_history(array &$variables) {

  // Fetch UserHistory Entity Object.
  /** @var \Drupal\user_history\Entity\UserHistoryInterface $entity */
  $entity = $variables['elements']['#user_history'];
  // Provide the label.
  $variables['label'] = $entity->label();
  // Provide the alias.
  $variables['url'] = $entity->toUrl()->toString();

  // Helpful $content variable for templates.
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

}
