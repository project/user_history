<?php

namespace Drupal\Tests\user_history\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group user_history
 */
class LoadFrontPageTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user_history',
    'node',
    'views',
  ];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * The default theme to use with the test.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set the front page to "/node".
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('page.front', '/node')
      ->save(TRUE);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoadFrontPage() {
    $this->drupalGet(Url::fromRoute('<front>'));
    // Confirm that the page loaded correctly.
    $this->assertSession()->statusCodeEquals(200);
    // Confirm that the front page contains the standard text.
    $this->assertSession()->pageTextContains('Welcome to Drupal');
  }

}
